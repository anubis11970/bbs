# -*- coding: utf-8 -*-
Encoding.default_external = 'UTF-8'
require 'sinatra'
require 'sinatra/reloader'
require 'mongo'


before do
	db = Mongo::Client.new([ '127.0.0.1:27017'], :database => 'bbs')
	@table = db[:bbsInfo]
end

helpers do
	#エスケープ処理
	include Rack::Utils
	alias_method :h, :escape_html

	#brタグに変換
	def convertBr(str)
		str.gsub(/\r\n|\r|\n/,'<br />')
	end
end


get '/' do
	erb :index
end

post '/posting'do
	postDate = Time.now.strftime("%Y-%m-%d %H:%M:%S")
	@table.insert_one({:name => params[:name],:comments => params[:comments],:date => postDate})
	redirect '/'
end

